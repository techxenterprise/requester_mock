import React from 'react';
import PropTypes from 'prop-types';

import { Avatar } from 'antd';

import './FlightCardHeader.css';

function FlightCardHeader({ operatorName, flightId, ...others }) {
  return (
    <header className="FlightCardHeader" {...others}>
      <div className="FlightCardHeader__avatar-wrapper">
        <Avatar icon="user" />
        <span className="FlightCardHeader__avatar-wrapper__operator-name">
          {operatorName}
        </span>
      </div>
      <span>Id: {flightId}</span>
    </header>
  );
}

FlightCardHeader.propTypes = {
  operatorName: PropTypes.string.isRequired,
  flightId: PropTypes.string.isRequired,
};

export default FlightCardHeader;
