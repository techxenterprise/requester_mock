import React from 'react';
import PropTypes from 'prop-types';

import { Card } from 'antd';
import FlightCardHeader from './FlightCardHeader';
import FlightCardContent from './FlightCardContent';

import './styles.css';

function FlightCard({ flight, ...others }) {
  const {
    operatorName,
    flightId,
  } = flight;

  return (
    <Card
      className="FlightCard"
      hoverable
      title={<FlightCardHeader operatorName={operatorName} flightId={flightId} />}
      {...others}
    >
      <FlightCardContent flight={flight} />
    </Card>
  );
}

FlightCard.propTypes = {
  flight: PropTypes.shape({
    operatorName: PropTypes.string,
    flightId: PropTypes.string,
    departure: PropTypes.shape({
      icao: PropTypes.string,
      cityName: PropTypes.string,
      time: PropTypes.string,
    }),
    arrival: PropTypes.shape({
      icao: PropTypes.string,
      cityName: PropTypes.string,
      time: PropTypes.string,
    }),
    passengers: PropTypes.number,
    aircraftName: PropTypes.string,
  }).isRequired,
};

export default FlightCard;
