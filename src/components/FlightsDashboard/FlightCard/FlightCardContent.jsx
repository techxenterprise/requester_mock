import React from 'react';
import PropTypes from 'prop-types';

import { Icon } from 'antd';

import './FlightCardContent.css';

function FlightCardContent({ flight }) {
  const {
    departure,
    arrival,
    passengers,
    aircraftName,
    ...others
  } = flight;
  
  return (
    <div className="FlightCardContent" {...others}>
      <div className="FlightCardContent__departure">
        <span className="icao">{departure.icao}</span>
        <span className="city-name">{departure.cityName}</span>
        <span>{departure.time}</span>
      </div>
      <Icon type="arrow-right" style={{ fontSize: 36 }} />
      <div className="FlightCardContent__arrival">
        <span className="icao">{arrival.icao}</span>
        <span className="city-name">{arrival.cityName}</span>
        <span>{arrival.time}</span>
      </div>
      <div className="FlightCardContent__footer">
        <span>{`${passengers} ${`passenger`.concat(passengers > 1 ? 's' : '')}`}</span>
        <span>{aircraftName}</span>
      </div>
    </div>
  );
}

FlightCardContent.propTypes = {
  flight: PropTypes.shape({
    departure: PropTypes.shape({
      icao: PropTypes.string,
      cityName: PropTypes.string,
      time: PropTypes.string,
    }),
    arrival: PropTypes.shape({
      icao: PropTypes.string,
      cityName: PropTypes.string,
      time: PropTypes.string,
    }),
    passengers: PropTypes.number,
    aircraftName: PropTypes.string,
  }).isRequired,
};

export default FlightCardContent;
