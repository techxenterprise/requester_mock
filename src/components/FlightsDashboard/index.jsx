import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { Row, Col, List } from 'antd';
import FilterForm from './FilterForm';
import FlightCard from './FlightCard';
import axios from 'axios';
import Modal from 'simple-react-modal';


import './styles.css';

// Mocked Data
let data = [
  {
    flight: {
      operatorName: 'Operator1',
      flightId: '12345',
      departure: {
        icao: 'ICAO',
        cityName: 'City Name Long',
        time: '9 Jun - 00:00',
      },
      arrival: {
        icao: 'ICAO',
        cityName: 'City Name Long',
        time: '9 Jun - 00:00',
      },
      passengers: 9,
      aircraftName: 'GL6000',
    },
  },
];
// End Mocked data

class FlightsDashboard extends Component {

  state = {
    show: true,
    message: "Loading",
    filter: {
      requesterId: "12351",
    },
    data: [
      {
        flight: {
          operatorName: '-------',
          flightId: '-------',
          departure: {
            icao: '-------',
            cityName: '-------',
            time: '-------',
          },
          arrival: {
            icao: '-------',
            cityName: '-------',
            time: '-------',
          },
          passengers: 0,
          aircraftName: '-------',
        },
      },
    ],
  }

  handleFilter(values = {}) {
    console.log('values: ', values);
  }

  componentDidMount() {
    this.backEndCall();
  }

  sleep (time) {
    return new Promise((resolve) => setTimeout(resolve, time));
  }

  backEndCall = () => {
    axios.post('http://localhost:8080/1/booking/get', this.state.filter)
      .then((response) => {
        this.setState({ message: " Waiting for Operator response" });
        this.sleep(3000).then(() => {
          axios.get('http://localhost:9001/1/toUI/Broker1')
            .then((response) => {
              data = [];
              response.data.bookingBody.forEach((booking) => {
                booking.flightLegs.forEach((flight) => {
                  data.push({
                    flight: {
                      operatorName: booking.operatorId,
                      flightId: booking.bookingReference,
                      departure: {
                        icao: flight.departureIcaoCode,
                        cityName: flight.departureIataCode,
                        time: flight.scheduleTimeOfDepartureLocal,
                      },
                      arrival: {
                        icao: flight.arrivalIcaoCode,
                        cityName: flight.arrivalIataCode,
                        time: flight.scheduleTimeOfArrivalLocal,
                      },
                      passengers: '0',
                      aircraftName: flight.tailNumber,
                    },
                  });
                });
              });
              this.setState({ data: data});
              this.setState({ show: false });
            });
        });
      })
      .catch((error) => {
        console.log(error);
        this.setState({message: error.data});
      });
  }

  render() {
    return (
      <section className="FlightsDashboard">
        <Modal show={this.state.show} onClose={this.state.closeModal}>
          <div>{this.state.message}</div>
        </Modal>
        <Row className="FlightsDashboard__filter-form">
          <FilterForm onFilter={this.handleFilter} />
        </Row>
        <List
          className="FlightsDashboard__flights-list"
          grid={{ gutter: 16, xs: 1, sm: 1, md: 2, lg: 3 }}
          dataSource={this.state.data}
          renderItem={item => (
            <List.Item>
              <FlightCard
                operatorName={item.operatorName}
                flightId={item.flightId}
                flight={item.flight}
              />
            </List.Item>
          )}
        >
        </List>
      </section>
    );
  }
}

export default FlightsDashboard;
