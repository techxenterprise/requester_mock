import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  Row,
  Col,
  Form,
  Input,
  Select,
  DatePicker,
  Button,
} from 'antd';

import './styles.css';

class FilterForm extends Component {
  static propTypes = {
    form: PropTypes.shape({
      getFieldDecorator: PropTypes.func,
      validateFields: PropTypes.func,
      resetFields: PropTypes.func,

    }).isRequired,
    onFilter: PropTypes.func.isRequired,
  };

  componentDidMount() {
    this.props.form.validateFields();
  }

  handleFilter = (e) => {
    e.preventDefault();

    const { form, onFilter } = this.props;

    form.validateFields((err, values) => {
      if (!err) {
        onFilter(values);
      }
    });
  }

  handleReset = () => {
    this.props.form.resetFields();
  }

  render() {
    const {
      getFieldDecorator,
      isFieldTouched,
      getFieldError,
    } = this.props.form;

    const FormItem = Form.Item;
    const Option = Select.Option;

    return (
      <Form
        className="FilterForm"
        layout="inline"
        onSubmit={this.handleFilter}
      >
        <Row gutter={32}>
          <Col span={5}>
            <FormItem
              className="FilterForm__row__item"
              label="Order Reference ID"
              colon={false}
            >
              {getFieldDecorator('orderReferenceId', {})(<Input />)}
            </FormItem>
          </Col>
          <Col span={5}>
            <FormItem
              className="FilterForm__row__item"
              label="Operator"
              colon={false}
            >
              {
                getFieldDecorator('operator', {})(
                  <Select placeholder="Please select an Operator">
                    <Option value="john doe">John Doe</Option>
                    <Option value="wesley nobody">Wesley Nobady</Option>
                    <Option value="harry potter">Harry Potter</Option>
                  </Select>
                )
              }
            </FormItem>
          </Col>
          <Col span={5}>
            <FormItem
              className="FilterForm__row__item"
              label="Operator Order ID"
              colon={false}
            >
              {getFieldDecorator('operatorOrderId', {})(<Input />)}
            </FormItem>
          </Col>
          <Col span={5}>
            <FormItem
              className="FilterForm__row__item"
              label="Operator Leg ID" colon= {false}
            >
              {getFieldDecorator('operatorLegId', {})(<Input />)}
            </FormItem>
          </Col>
        </Row>
        <Row type="flex" gutter={32}>
          <Col span={5}>
            <FormItem
              className="FilterForm__row__item"
              label="Order ID"
              colon={false}
            >
              {getFieldDecorator('orderId', {})(<Input />)}
            </FormItem>
          </Col>
          <Col span={5}>
            <FormItem
              className="FilterForm__row__item"
              label="Leg ID"
              colon={false}
            >
              {getFieldDecorator('legId', {})(<Input />)}
            </FormItem>
          </Col>
          <Col span={5}>
            <FormItem
              className="FilterForm__row__item"
              label="Tail Number"
              colon={false}
            >
              {getFieldDecorator('tailNumber', {})(<Input />)}
            </FormItem>
          </Col>
          <Col span={5}>
            <FormItem
              className="FilterForm__row__item"
              label="Aircraft Type Code" colon={false}
            >
              {getFieldDecorator('aircraftTypeCode', {})(<Input />)}
            </FormItem>
          </Col>
        </Row>
        <Row type="flex" align="bottom" gutter={32}>
          <Col span={3}>
            <FormItem
              className="FilterForm__row__item"
              label="Departure Date"
              colon={false}
            >
              {getFieldDecorator('departureDate', {})(<DatePicker placeholder="" />)}
            </FormItem>
          </Col>
          <Col span={3}>
            <FormItem
              className="FilterForm__row__item"
              label="Departure IATA"
              colon={false}
            >
              {getFieldDecorator('departureIATA', {})(<DatePicker placeholder="" />)}
            </FormItem>
          </Col>
          <Col span={3}>
            <FormItem
              className="FilterForm__row__item"
              label="Departure ICAO"
              colon={false}
            >
              {getFieldDecorator('departureICAO', {})(<DatePicker placeholder="" />)}
            </FormItem>
          </Col>
          <Col span={3}>
            <FormItem
              className="FilterForm__row__item"
              label="Arrival Date"
              colon={false}
            >
              {getFieldDecorator('arrivalDate', {})(<DatePicker placeholder="" />)}
            </FormItem>
          </Col>
          <Col span={3}>
            <FormItem
              className="FilterForm__row__item"
              label="Arrival IATA"
              colon={false}
            >
              {getFieldDecorator('arrivalIATA', {})(<DatePicker placeholder="" />)}
            </FormItem>
          </Col>
          <Col span={3}>
            <FormItem
              className="FilterForm__row__item"
              label="Arrival ICAO"
              colon={false}
            >
              {getFieldDecorator('arrivalICAO', {})(<DatePicker placeholder="" />)}
            </FormItem>
          </Col>
          <Col span={2} offset={2}>
            <Button onClick={this.handleReset} className="FilterForm__button">
              Reset
            </Button>
          </Col>
          <Col span={2}>
            <Button
              className="FilterForm__button"
              icon="filter"
              type="primary"
              htmlType="submit"
            >
              Filter
            </Button>
          </Col>
        </Row>
      </Form>
    );
  }
}

export default Form.create()(FilterForm);
