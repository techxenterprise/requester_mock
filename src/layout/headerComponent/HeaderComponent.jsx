import React from 'react';
import { Avatar, Badge, Col, Dropdown, Icon, Layout, Menu, Row } from 'antd';
import {Link} from 'react-router-dom';

const { Header } = Layout;

const menu = (
  <Menu>
    <Menu.Item key="0">
      <a target="blank" href="http://www.techx.com.br">1st menu item</a>
    </Menu.Item>
    <Menu.Item key="1">
      <a target="blank" href="http://www.techx.com.br">2nd menu item</a>
    </Menu.Item>
    <Menu.Divider />
    <Menu.Item key="3">3rd menu item</Menu.Item>
  </Menu>
);

const date = (
  <div className="user-date">
    <div>
      UTC 19:14
    </div>
    <div>
      Aug Thu 17
    </div>
  </div>
);

const HeaderComponent = () => (
  <Header className="custom-header">
    <Row>
      <Col xs={24} md={16}>
        <Menu
          theme="light"
          mode="horizontal"
          defaultSelectedKeys={['1']}
          style={{ lineHeight: '64px' }}
        >
          <Menu.Item key="1">
            <Link to="/">All Flights</Link>
          </Menu.Item>
          <Menu.Item key="2">
            <Link to="/create-order">Create an Order</Link>
          </Menu.Item>
        </Menu>
      </Col>
      <Col xs={24} md={8} style={{ display: 'flex', justifyContent: 'flex-end' }}>
        <span>
          <Badge count={3}>
            {date}
            <Avatar style={{ backgroundColor: '#1496F4' }} icon="user" />
          </Badge>
        </span>
        <div>
          <Dropdown overlay={menu} trigger={['click']}>
            <span style={{ marginLeft: '16px' }}>
              <Icon type="down" />
            </span>
          </Dropdown>
        </div>
      </Col>
    </Row>
  </Header>
);

export default HeaderComponent;
