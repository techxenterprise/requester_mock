import React, { Component } from 'react';
import { Button, Card, Col, DatePicker, Input, Row } from 'antd';

class TabContent extends Component {

  state = {
    scheduledDepLocalTime: null,
    scheduledDepUTCTime: null,
    estimatedDepUTCTime: null,
    scheduledArrLocalTime: null,
    scheduledArrUTCTime: null,
    estimatedArrLocalTime: null,
    estimatedArrUTCTime: null,
  };
  disabledEndDate = (endValue) => {
    return false;
  }
  disabledStartDate = (startValue) => {
    return false;
  }
  onChange = (field, value) => {
    this.setState({
      [field]: value,
    });
  }

  onEstimatedDepLocalChange = (value) => {
    this.onChange('estimatedDepLocalTime', value);
  }
  onEstimatedArrLocalChange = (value) => {
    this.onChange('estimatedArrLocalTime', value);
  }
  onEstimatedDepUTCChange = (value) => {
    this.onChange('estimatedDepUTCTime', value);
  }
  onEstimatedArrUTCChange = (value) => {
    this.onChange('estimatedArrUTCTime', value);
  }
  onScheduledDepLocalChange = (value) => {
    this.onChange('scheduledDepLocalTime', value);
  }
  onScheduledArrLocalChange = (value) => {
    this.onChange('scheduledArrLocalTime', value);
  }
  onScheduledDepUTCChange = (value) => {
    this.onChange('scheduledDepUTCTime', value);
  }
  onScheduledArrUTCChange = (value) => {
    this.onChange('scheduledArrUTCTime', value);
  }
  parseDate = (moment) => {
    return moment;
  }

  render() {
    return (
      <div className="card-tab">
        <Row style={{ marginBottom: '20px' }}>
          <Col span={7} style={{ marginRight: '15px' }}>
            <div className="label">
              Booking ID
            </div>
            <Input placeholder="Booking ID" onChange={(e) => this.props.handler("requesterBookingId", e.target.value, this.props.index)}/>
          </Col>
          <Col span={7} style={{ marginRight: '15px' }}>
            <div className="label">
              Leg ID
            </div>
            <Input onChange={(e) => this.props.handler("requesterLegId", e.target.value, this.props.index)} placeholder="Leg ID"/>
          </Col>
          <Col span={7}>
            <div className="label">
              Tail Number
            </div>
            <Input onChange={(e) => this.props.handler("tailNumber", e.target.value, this.props.index)} placeholder="Tail Number"/>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <Card title="DEPARTURE" style={{ marginBottom: '20px', marginRight: '15px' }}>
              <Row style={{ marginBottom: '20px' }}>
                <Col span={11} style={{ marginRight: '15px' }}>
                  <div className="label">
                    Airport IATA
                  </div>
                  <Input onChange={(e) => this.props.handler("departureIataCode", e.target.value, this.props.index)} placeholder="Airport IATA Code"/>
                </Col>
                <Col span={11}>
                  <div className="label">
                    Airport ICAO
                  </div>
                  <Input onChange={(e) => this.props.handler("departureIcaoCode", e.target.value, this.props.index)} placeholder="Airport ICAO Code"/>
                </Col>
              </Row>
              <Row style={{ marginBottom: '20px' }}>
                <Col span={11} style={{ marginRight: '15px' }}>
                  <div className="label">
                    ETD Local
                  </div>
                  <DatePicker
                    disabledDate={this.disabledStartDate}
                    showTime
                    format="YYYY-MM-DD HH:mm"
                    placeholder="Start"
                    onChange={(e) => this.props.handler("estimatedTimeOfDepartureLocal", this.parseDate(e), this.props.index)}
                  />
                </Col>
                <Col span={11}>
                  <div className="label">
                    ETD UTC
                  </div>
                  <DatePicker
                    disabledDate={this.disabledStartDate}
                    showTime
                    format="YYYY-MM-DD HH:mm"
                    placeholder="Start"
                    onChange={(e) => this.props.handler("estimatedTimeOfDepartureUtc", this.parseDate(e), this.props.index)}
                  />
                </Col>
              </Row>
              <Row style={{ marginBottom: '20px' }}>
                <Col span={11} style={{ marginRight: '15px' }}>
                  <div className="label">
                    STD Local
                  </div>
                  <DatePicker
                    disabledDate={this.disabledStartDate}
                    showTime
                    format="YYYY-MM-DD HH:mm"
                    placeholder="Start"
                    onChange={(e) => this.props.handler("scheduleTimeOfDepartureLocal", this.parseDate(e), this.props.index)}
                  />
                </Col>
                <Col span={11}>
                  <div className="label">
                    STD UTC
                  </div>
                  <DatePicker
                    disabledDate={this.disabledStartDate}
                    showTime
                    format="YYYY-MM-DD HH:mm"
                    placeholder="Start"
                    onChange={(e) => this.props.handler("scheduleTimeOfDepartureUtc", this.parseDate(e), this.props.index)}
                  />
                </Col>
              </Row>
            </Card>
          </Col>
          <Col span={12}>
            <Card title="ARRIVAL" style={{ marginBottom: '20px', marginLeft: '15px' }}>
              <Row style={{ marginBottom: '20px' }}>
                <Col span={11} style={{ marginRight: '15px' }}>
                  <div className="label">
                    Airport IATA
                  </div>
                  <Input onChange={(e) => this.props.handler("arrivalIataCode", e.target.value, this.props.index)} placeholder="Airport IATA Code"/>
                </Col>
                <Col span={11}>
                  <div className="label">
                    Airport ICAO
                  </div>
                  <Input onChange={(e) => this.props.handler("arrivalIcaoCode", e.target.value, this.props.index)} placeholder="Airport ICAO Code"/>
                </Col>
              </Row>
              <Row style={{ marginBottom: '20px' }}>
                <Col span={11} style={{ marginRight: '15px' }}>
                  <div className="label">
                    ETA Local
                  </div>
                  <DatePicker
                    disabledDate={this.disabledEndDate}
                    showTime
                    format="YYYY-MM-DD HH:mm"
                    placeholder="End"
                    onChange={(e) => this.props.handler("estimatedTimeOfArrivalLocal", this.parseDate(e), this.props.index)}
                  />
                </Col>
                <Col span={11}>
                  <div className="label">
                    ETA UTC
                  </div>
                  <DatePicker
                    disabledDate={this.disabledEndDate}
                    showTime
                    format="YYYY-MM-DD HH:mm"
                    placeholder="End"
                    onChange={(e) => this.props.handler("estimatedTimeOfArrivalUtc", this.parseDate(e), this.props.index)}
                  />
                </Col>
              </Row>
              <Row style={{ marginBottom: '20px' }}>
                <Col span={11} style={{ marginRight: '15px' }}>
                  <div className="label">
                    STA Local
                  </div>
                  <DatePicker
                    disabledDate={this.disabledEndDate}
                    showTime
                    format="YYYY-MM-DD HH:mm"
                    placeholder="End"
                    onChange={(e) => this.props.handler("scheduleTimeOfArrivalLocal", this.parseDate(e), this.props.index)}
                  />
                </Col>
                <Col span={11}>
                  <div className="label">
                    STA UTC
                  </div>
                  <DatePicker
                    disabledDate={this.disabledEndDate}
                    showTime
                    format="YYYY-MM-DD HH:mm"
                    placeholder="End"
                    onChange={(e) => this.props.handler("scheduleTimeOfArrivalUtc", this.parseDate(e), this.props.index)}
                  />
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
        <Row style={{ marginBottom: '20px' }}>
          <Col span={11} style={{ marginRight: '15px' }}>
            <div className="label">
              Note
            </div>
            <Input.TextArea onChange={(e) => this.props.handler("notes", e.target.value, this.props.index)} rows={4} placeholder="Type here" />
          </Col>
          <Col span={11} style={{ marginLeft: '15px' }}>
            <div className="label">
              Remark
            </div>
            <Input.TextArea onChange={(e) => this.props.handler("remarks", e.target.value, this.props.index)} rows={4} placeholder="Type here" />
          </Col>
        </Row>
        <Row>
          <Col span={24} style={{textAlign: 'right'}}>
            <Button style={{ marginRight: '15px' }} type="default">Reset</Button>
            <Button type="primary" onClick={this.props.submit} >Save</Button>
          </Col>
        </Row>
      </div>
    );
  }
}


export default TabContent;
