/* eslint-disable no-console */
import React, { Component } from 'react';
import { Button, Card, Col, Input, Select } from 'antd';

class BookingCard extends Component {

  handleBlur = () => {
    console.log('blur');
  }
  handleFocus = () => {
    console.log('focus');
  }
  render() {
    return (
      <Col span={6} style={{ marginRight: '25px' }}>
        <Card className="margin-bottom-16">
          <div className="label">
            Booking Reference ID
          </div>
          <Input onChange={(e) => this.props.handler("bookingReference", e.target.value)}   placeholder="Booking Reference ID" style={{ marginBottom: '20px' }} />
          <div className="label">
            Operator
          </div>
          <Select
            showSearch
            style={{ width: '100%', marginBottom: '20px' }}
            optionFilterProp="children"
            onChange={(e) => this.props.handler("operatorId", e)}
            onFocus={this.handleFocus}
            onBlur={this.handleBlur}
            defaultValue="VISTAJET"
            filterOption={
              (input, option) => option.props.children
                .toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
          >
            <Select.Option value="VISTAJET">VISTAJET</Select.Option>
            <Select.Option value="JET_AVIATION">JET AVIATION</Select.Option>
          </Select>
          <div className="label">
            Notes
          </div>
          <Input.TextArea onChange={(e) => this.props.handler("notes", e.target.value)} rows={6} placeholder="Notes" style={{marginBottom: '20px'}}/>
        </Card>
      </Col>
    );
  }
}
export default BookingCard;
