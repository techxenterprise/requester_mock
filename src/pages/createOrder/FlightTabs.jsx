import React, {Component} from 'react';
import { Tabs, Col } from 'antd';
import TabContent from './TabContent';


class FlightTabs extends Component {
  render() {
    return (
      <Col span={16}>
        <Tabs type="card">
          <Tabs.TabPane tab="ICAO - ICAO" key="1">
            <TabContent handler={this.props.handler} submit={this.props.submit} index={0} />
          </Tabs.TabPane>
          <Tabs.TabPane tab="FROM - TO" key="2">
            <TabContent handler={this.props.handler} submit={this.props.submit} index={1} />
          </Tabs.TabPane>
          <Tabs.TabPane tab="+ Legs" key="3">
            <TabContent handler={this.props.handler} submit={this.props.submit} index={2} />
          </Tabs.TabPane>
        </Tabs>
      </Col>
    );
  }
}

export default FlightTabs;
