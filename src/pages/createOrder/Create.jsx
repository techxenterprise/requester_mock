import React, {Component} from 'react';
import { Row } from 'antd';
import BookingCard from './BookingCard';
import FlightTabs from './FlightTabs';
import axios from 'axios';
import Modal from 'simple-react-modal';

class Home extends Component {
  state = {
    show: false,
    message: "Sending form to the operator ...",
    book: {
      bookingDateUTC: "2018-10-18T09:15:15Z",
      bookingReference: "",
      requesterId: "BrokerA",
      notes: "",
      operatorId: "VISTAJET",
      flightLegs: [
        {
          bookingID: "",
          arrivalIataCode: "",
          arrivalIcaoCode: "",
          requesterBookingId: "",
          requesterLegId: "",
          departureIataCode: "",
          departureIcaoCode: "",
          estimatedTimeOfArrivalLocal: "",
          estimatedTimeOfArrivalUtc: "",
          estimatedTimeOfDepartureLocal: "",
          estimatedTimeOfDepartureUtc: "",
          notes: "",
          operatorBookingId: "",
          operatorLegId: "",
          remarks: "",
          scheduleTimeOfArrivalLocal: "",
          scheduleTimeOfArrivalUtc: "",
          scheduleTimeOfDepartureLocal: "",
          scheduleTimeOfDepartureUtc: "",
          tailNumber: "",
          aircraftTypeCode: "A320",
          aircraftTypeDisplayName: "Airbus 320",
          aircraftTypeName: "Airbus A320neo",
        },
      ],
    },
  }

  handleBookChange = (id, value) => {
    const { book } = this.state;
    book[id] = value;
    this.setState({ book: book});
    console.log(this.state);
  }

  handleFlightLegChange = (id, value, index) => {
    const legs = this.state.book.flightLegs;
    legs[index][id] = value;
    this.setState({
      book: {
        ...this.state.book,
        flightLegs: legs,
      },
    })
    console.log(this.state.book.flightLegs);
  }

  openModal(){
    this.setState({show: true});
  }

  closeModal(){
    this.setState({show: false});
  }

  formHandler = () => {
    this.openModal();
    axios.post('http://localhost:8080/1/booking/bookingStart', this.state.book)
      .then((response) => {
        console.log(response);
        this.setState({ message: response.data.header.statusDescription + " -  Booking sent to the bus" });
      })
      .catch((error) => {
        this.setState({ message: 'Some error Ocurred Please check all data again'});
      });
  }

  render() {
    return (
      <div>
        <Modal show={this.state.show} onClose={this.state.closeModal}>
          <div>{this.state.message}</div>
        </Modal>
        <Row gutter={24}>
          <BookingCard handler={this.handleBookChange} />
          <FlightTabs handler={this.handleFlightLegChange} submit={this.formHandler} />
        </Row>
      </div>
    );
  }
}

export default Home;
