import React from 'react';
import FlightsDashboard from '../components/FlightsDashboard';

import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';

import { Layout } from 'antd';

import Header from '../layout/headerComponent/HeaderComponent';
import Footer from '../layout/footerComponent/FooterComponent';

import Home from './home/Home';
import NotFound from './notFound/NotFound';

import '../assets/styles/styles.css';
import CreateOrder from './createOrder/Create';

const { Content } = Layout;

const Main = () => (
  <Router>
    <Layout style={{ minHeight: '100vh' }}>
      <Layout>
        <Header />
        <Content>
          <Switch>
            <Route exact path="/" component={FlightsDashboard} />
            <Route path="/create-order" component={CreateOrder} />
            <Route component={NotFound} />
          </Switch>
        </Content>
        <Footer />
      </Layout>
    </Layout>
  </Router>
);

export default Main;
